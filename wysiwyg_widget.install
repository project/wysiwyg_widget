<?php

/**
 * @file
 * Install hooks for Wysiwyg Widget project.
 */


/**
 * Implements hook_requirements().
 */
function wysiwyg_widget_requirements($phase) {
  $requirements = array();
  $t = get_t();

  if ($phase == 'runtime') {
    if ($modules = module_implements('wysiwyg_widget_embed_alter')) {
      $requirements['wysiwyg_widget'] = array(
        'title' => 'Wysiwyg Widget',
        'value' => $t('hook_wysiwyg_widget_embed_alter() is no longer supported. You can use Ctools hooks to alter the objects.'),
        'description' => $t('The following modules are still using the deprecated hook: !modules', array('!modules' => implode(',', $modules))),
        'severity' => REQUIREMENT_WARNING,
      );
    }

    return $requirements;
  }
}

/**
 * Implements hook_schema().
 */
function wysiwyg_widget_schema() {
  $schema['wysiwyg_widget'] = array(
    'description' => 'Stores Wysiwyg Widget Sets.',
    'export' => array(
      'primary key' => 'swid',
      'identifier' => 'wysiwyg_widget_set', // Exports will be available as $wysiwyg_widget_set
      'default hook' => 'default_wysiwyg_widget_set', // Function hook name.
      'api' => array(
        'owner' => 'wysiwyg_widget',
        'api' => 'default_wysiwyg_widget_set', // Base name for api include files.
        'minimum_version' => 1,
        'current_version' => 1,
      ),
      'load callback' => 'wysiwyg_widget_set_load',
    ),
    'fields' => array(
      'swid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary ID field for the table. Not used for anything except internal look ups.',
        'no export' => TRUE, // Do not export database-only keys.
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'pattern' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'flags' => array(
        'type' => 'varchar',
        'length' => 5,
        'not null' => TRUE,
        'default' => 'gi',
      ),
      'external_js' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'external_js_async' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
        'unsigned' => TRUE,
      ),
    ),
    'primary key' => array('swid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function wysiwyg_widget_uninstall() {
  drupal_uninstall_schema('wysiwyg_widget');
  variable_del('wysiwyg_widget_xss_header');
  variable_del('wysiwyg_widget_widget_embed_placeholders_only');
}

/**
 * Create wysiwyg_widget table and populate values from hook.
 */
function wysiwyg_widget_update_7001() {
  ctools_include('export');

  // Create database if needed.
  if (db_table_exists('wysiwyg_widget') == FALSE) {
    drupal_install_schema('wysiwyg_widget');
  }

  // Import configuration from hooks.
  $placeholders = array();
  drupal_alter('wysiwyg_widget_embed', $placeholders);
  if (!empty($placeholders)) {
    foreach ($placeholders as $key => $placeholder) {
      $name = str_replace('-', ' ', $key);
      $name = str_replace('_', ' ', $name);

      $item = new stdClass();
      $item->swid = NULL;
      $item->name = ucfirst($name);
      $item->pattern = isset($placeholder['regex']['pattern']) ? $placeholder['regex']['pattern'] : '';
      $item->flags = isset($placeholder['regex']['flags']) ? $placeholder['regex']['flags'] : '';
      $item->type = 'Normal';
      $item->export_type = NULL;

      ctools_export_crud_save('wysiwyg_widget', $item);
      drupal_set_message(t('Imported Widget Embed placeholder: :name', array(':name' => $item->name)));
    }
  }
}
